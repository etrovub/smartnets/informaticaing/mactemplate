#include "drawing.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

color WHITE = {.r = 255, .g = 255, .b = 255, .a = 255};
color RED = {.r = 255, .g = 0, .b = 0, .a = 255};
color GREEN = {.r = 0, .g = 255, .b = 0, .a = 255};
color BLUE = {.r = 0, .g = 0, .b = 255, .a = 255};
color BLACK = {.r = 0, .g = 0, .b = 0, .a = 255};

int create_window(const char *name, int width, int height);
int draw_empty_circle(int center_x, int center_y, int radius);
int fill_circle(int center_x, int center_y, int radius);

SDL_Window* window;
SDL_Renderer* renderer;

int window_width;
int window_height;

int create_window(const char *name, int width, int height) {
    if (window != NULL) {
            return 1;
    }
    window_height = height;
    window_width = width;
    window = SDL_CreateWindow(name,
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              window_height,
                              window_width,
                              0
                              );
    renderer = SDL_CreateRenderer(window, -1, 0);
    TTF_Init();
    return 0;
}

int wait(int ms) {
    SDL_Delay(ms);
    return 0;
}

int draw_background(color c) {
    if (renderer == NULL) {
        return 1;
    }
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    SDL_RenderClear(renderer);
    return 0;
}

int draw_point(int x, int y, color c) {
    if (renderer == NULL) {
        return 1;
    }
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    SDL_RenderDrawPoint(renderer, x, y);
    return 0;
}

int draw_line(int x1, int y1, int x2, int y2, color c) {
    if (renderer == NULL) {
        return 1;
    }
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
    return 0;
}

//https://gist.github.com/Gumichan01/332c26f6197a432db91cc4327fcabb1c
int draw_circle(int x, int y, int radius, color c, bool fill) {
    if (renderer == NULL) {
        return 1;
    }
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    if(fill) {
        return fill_circle(x, y, radius);
    } else {
        return draw_empty_circle(x, y, radius);
    }
}

int draw_empty_circle(int center_x, int center_y, int radius) {
    int diameter = (radius * 2);
    int x = (radius - 1);
    int y = 0;
    int tx = 1;
    int ty = 1;
    int error = (tx - diameter);

    while (x >= y)
    {
      //  Each of the following renders an octant of the circle
      SDL_RenderDrawPoint(renderer, center_x + x, center_y - y);
      SDL_RenderDrawPoint(renderer, center_x + x, center_y + y);
      SDL_RenderDrawPoint(renderer, center_x - x, center_y - y);
      SDL_RenderDrawPoint(renderer, center_x - x, center_y + y);
      SDL_RenderDrawPoint(renderer, center_x + y, center_y - x);
      SDL_RenderDrawPoint(renderer, center_x + y, center_y + x);
      SDL_RenderDrawPoint(renderer, center_x - y, center_y - x);
      SDL_RenderDrawPoint(renderer, center_x - y, center_y + x);
      if (error <= 0)
      {
         ++y;
         error += ty;
         ty += 2;
      }
      if (error > 0)
      {
         --x;
         tx += 2;
         error += (tx - diameter);
      }
    }
    return 0;
}

int fill_circle(int x, int y, int radius) {
    int offsetx, offsety, d;
    int status;
    offsetx = 0;
    offsety = radius;
    d = radius -1;
    status = 0;

    while (offsety >= offsetx) {
        status += SDL_RenderDrawLine(renderer, x - offsety, y + offsetx,
                                     x + offsety, y + offsetx);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y + offsety,
                                     x + offsetx, y + offsety);
        status += SDL_RenderDrawLine(renderer, x - offsetx, y - offsety,
                                     x + offsetx, y - offsety);
        status += SDL_RenderDrawLine(renderer, x - offsety, y - offsetx,
                                     x + offsety, y - offsetx);

        if (status < 0) {
            status = -1;
            break;
        }
        if (d >= 2*offsetx) {
            d -= 2*offsetx + 1;
            offsetx +=1;
        }
        else if (d < 2 * (radius - offsety)) {
            d += 2 * offsety - 1;
            offsety -= 1;
        }
        else {
            d += 2 * (offsety - offsetx - 1);
            offsety -= 1;
            offsetx += 1;
        }
    }
    return status;
}

int draw_rectangle(int x, int y, int w, int h, color c, bool fill) {
    if (renderer == NULL) {
        return 1;
    }
    SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
    SDL_Rect rect = {.x=x, .y=x, .w=w, .h=h};
    if (fill) {
        SDL_RenderFillRect(renderer, &rect);
    } else {
        SDL_RenderDrawRect(renderer, &rect);
    }
    return 0;
}

int draw_text(int x, int y, int size, color c, const char* text) {
    if (renderer == NULL) {
        return 1;
    }
    int w, h;
    TTF_Font *font = TTF_OpenFont("sans.ttf", size);
    TTF_Init();
    if(!font) {
        printf("Could not load font! Error: %s\n", SDL_GetError());
        return -1;
    }
    SDL_Color fg = {c.r, c.g, c.b, c.a};
    SDL_Color bg = {0, 0, 0, 0};
    TTF_SizeText(font, text, &w, &h);

    SDL_Surface* textSurface = TTF_RenderText_Shaded(font, text, fg, bg);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, textSurface);


    SDL_Rect rect = {.x=x, .y=x, .w=w, .h=h};

    SDL_RenderCopy(renderer, texture, NULL, &rect);

    SDL_FreeSurface(textSurface);
    SDL_DestroyTexture(texture);
    return 0;

}

int cleanup() {
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
    return 0;
}

int show() {
    SDL_RenderPresent(renderer);
    return 0;
}

int pause() {
    int running = 1;
    SDL_Event event;
    while (running) {
        SDL_WaitEvent(&event);
        switch(event.type) {
        case SDL_QUIT:
            running = 0;
        case SDL_KEYDOWN:
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                running = 0;
            }
        }
    }
    return 1;
}
