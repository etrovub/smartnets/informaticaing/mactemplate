#include "./drawing.h"

int main() {
    create_window("Window", 1000, 1000);
    draw_background(BLACK);
    draw_text(100, 100, 40, WHITE, "HELLO WORLD!");
    show();
    wait(1000);
    draw_rectangle(200, 200, 100, 30, GREEN, 1);
    draw_rectangle(500, 500, 30, 100, GREEN, 0);
    show();
    wait(1000);
    draw_circle(500,500, 250, WHITE, 1);
    draw_circle(500,500, 100, BLUE, 0);
    show();
    wait(1000);
    draw_line(0,0,1000,1000, RED);
    show();
    wait(1000);
    draw_background(BLUE);
    show();
    pause();
    cleanup();
}

