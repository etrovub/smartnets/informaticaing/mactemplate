#ifndef DRAWING_HEADER
#include <stdbool.h>
#define DRAWING_HEADER
typedef struct color {
    char r;
    char g;
    char b;
    char a;
} color;

int create_window(const char *name, int width, int height);
int draw_background(color c);
int wait(int ms);
int draw_point(int x, int y, color c);
int draw_line(int x1, int y1, int x2, int y2, color c);
int draw_circle (int center_x, int center_y, int radius, color c, bool fill);
int draw_text(int x, int y, int size, color c, const char* text);
int draw_rectangle(int x, int y, int w, int h, color c, bool fill);
int show();
int pause();
int cleanup();


extern color WHITE, RED, GREEN, BLUE, BLACK;

#endif // DRAWING_HEADER
