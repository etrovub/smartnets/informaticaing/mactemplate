CC = clang
O = -g -Wall `sdl2-config --cflags --libs`

all:
	$(MAKE) main

drawing.o: drawing.c drawing.h
	$(CC) $(O) drawing.c -c -o drawing.o

main: main.c drawing.o
	$(CC) $(O) $^  -o main -lSDL2_ttf

clean:
	rm *.o main


